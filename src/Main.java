import java.io.*;
import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {

        try {
            // Input file
            FileInputStream fileInputStream = new FileInputStream("compressed.bin");
            ObjectInputStream is = new ObjectInputStream(fileInputStream);
            PrintWriter outFile = new PrintWriter(new FileWriter("uncompressed.txt"));

            // How many chars?
            int size = is.readInt();
            Node[] list = new Node[256];
            int nodeSize = 0;

            // read chars and their huffman code lengths
            for (int i = 0; i < size; i++) {
                char c = is.readChar();
                int l = is.readInt();
                list[nodeSize] = new Node(c, l);
                nodeSize++;
            }

            // Generate canonical codes
            for (int i = 0; i < list[0].length; i++) {
                list[0].canonicalCode[list[0].canonicalLength] = 0;
                list[0].canonicalLength++;
            }

            int i = 1;
            int[] temp = new int[256];
            while (i < nodeSize) {
                if (list[i].length == list[i - 1].length) {
                    int k = list[i - 1].canonicalLength - 1;
                    while (k >= 0) {
                        if (list[i - 1].canonicalCode[k] == 1) {
                            temp[k] = 0;
                            k--;
                        } else {
                            temp[k] = 1;
                            break;
                        }
                    }
                    for (int m = k - 1; m >= 0; m--) temp[m] = list[i - 1].canonicalCode[m];

                    for (int j = 0; j < list[i - 1].canonicalLength; j++) {
                        list[i].canonicalCode[list[i].canonicalLength] = temp[j];
                        list[i].canonicalLength++;
                    }
                } else {
                    int k = list[i - 1].canonicalLength - 1;
                    while (k >= 0) {
                        if (list[i - 1].canonicalCode[k] == 1) {
                            temp[k] = 0;
                            k--;
                        } else {
                            temp[k] = 1;
                            break;
                        }
                    }
                    for (int m = k - 1; m >= 0; m--) temp[m] = list[i - 1].canonicalCode[m];

                    for (int j = 0; j < list[i - 1].canonicalLength; j++) {
                        list[i].canonicalCode[j] = temp[j];
                        list[i].canonicalLength++;
                    }
                    for (int m = 0; m < list[i].length - list[i - 1].length; m++) {
                        list[i].canonicalCode[list[i].canonicalLength] = 0;
                        list[i].canonicalLength++;
                    }
                }
                i++;
            }

//            for (int k = 0; k < nodeSize; k++) {
//                System.out.print((int)list[k].c + " - ");
//                for (int j = 0; j < list[k].canonicalLength; j++) {
//                    System.out.print(list[k].canonicalCode[j]);
//                }
//                System.out.println();
//            }

            // Find code from bits and replace with corresponding char
            byte data = 0;
            int[] bits = new int[256];
            int bitsStart = 0;
            int bitsEnd = 0;
            int p = 0;
            while (is.available() > 0 || p < bitsEnd) {
                if (bitsEnd == 0 || p > bitsEnd) {

                    data = is.readByte();

                    byte bitCount = 8;
//                    if (is.available() == 1) {
//                        bitCount = is.readByte();
//                    }

                    for (int j = 7; j >= 8 - bitCount; j--) {
                        bits[bitsEnd] = (data & (1 << j)) != 0 ? 1 : 0;
                        bitsEnd++;
                    }
                }

                int q = 0;
                boolean con = true;
                while (con && q < nodeSize) {
                    if ((p - bitsStart) == list[q].canonicalLength) {
                        boolean foundMatch = true;
                        for (int n = bitsStart; n < p; n++) {
                            if (list[q].canonicalCode[n - bitsStart] != bits[n]) {
                                foundMatch = false;
                                break;
                            }
                        }
                        if (foundMatch) {

//                            System.out.print(list[q].c);
                            outFile.write(list[q].c);

                            for (int v = p; v < bitsEnd; v++) {
                                bits[v - p] = bits[v];
                            }

                            bitsEnd -= p;
                            p = 0;

                            break;
                        }
                    } else if ((p - bitsStart) < list[q].canonicalLength) con = false;
                    q++;
                }
                p++;
            }

            is.close();
            outFile.flush();
            outFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
