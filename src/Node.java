public class Node {
    char c;
    int length;
    int[] canonicalCode = new int[256];
    int canonicalLength;

    public Node(char c, int length) {
        this.c = c;
        this.length = length;
    }
}
